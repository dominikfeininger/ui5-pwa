(function () {

    var ModulePreload = function () {
        this.sModulePath = undefined;
        this.moduleName = undefined;
    };

    /**
    * Loads a resource to the preload cache of openui5
    * @param {String} library - library to load the resource from
    * @param {String} resource - rescoure to load from the library
    * @param {boolean} json - if true the resource is json file.
    *        This needs to be stringified before adding it to the cache
    * @param {String} dataType - jQuery dataType
    */
    ModulePreload.prototype.preloadModule = function (library, resource, json, dataType) {
        let sModulePath = jQuery.sap.getModulePath(library, resource);
        this.moduleName = library + resource;
        jQuery.get(sModulePath, function (data, textStatus, jqXHR) {
            if (json) {
                data = JSON.stringify(data);
            }
            var oData = {
                name: "test.Sample.thirdparty",
                modules: {
                },
                version: "2.0"
            };
            oData.modules[this.moduleName] = data;
            jQuery.sap.registerPreloadedModules(oData);
        }.bind(this), dataType);
    }
    // add the ModulePreload class to the global window object
    window.ModulePreload = ModulePreload;

})();
