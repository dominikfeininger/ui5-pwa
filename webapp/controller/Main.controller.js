sap.ui.define(["test/Sample/controller/BaseController", "sap/m/MessageToast"], (Controller, MessageToast) => {
	"use strict";

	return Controller.extend("test.Sample.controller.Main", {
		// (live) transpiling async functions to ES5 generators not yet doable in ui5-tooling ecosys :)
		/* async */ onInit() {
			// let response;
			// let oLatestUI5 = {
			//     version: "n/a"
			// };
			// try {
			//     response = await fetch('/proxy/api/v1/latest?format=json');
			//     oLatestUI5 = await response.json();
			// } catch (err) {
			//     console.error(err)
			// }
			// this.getModel('LatestUI5').setProperty("/latest", latestU5version.version)

			fetch("/proxy/api/v1/latest?format=json")
				.then((response) => response.json())
				.then((latestU5version) => {
					const latestUI5Model = this.getModel("LatestUI5");
					latestUI5Model.setProperty("/latest", latestU5version.version);
					latestUI5Model.setProperty("/type", latestU5version.type);
				})
				.catch((err) => console.error(err));

			fetch("docs/index.md")
				.then((response) => response.text())
				.then((content) => {
					this.byId("doc").setValue(content);
				})
				.catch((err) => console.error(err));

			this.registerSW()
		},

		checked: false,
		swRegistration: null,
		isSubscribed: false,
		applicationServerPublicKey: 'BPL7xM7aDSXOgyn3uQLVOrsEkWVKjbixCCUbYTEkImiadOR7b3ZD67X9BUdRPexY__kTqsnOfWzg90PRD-5nSSY',

		navFwd() {
			return this.getOwnerComponent().getRouter().navTo("RouteOther");
		},
		navShow3rd() {
			return this.getOwnerComponent().getRouter().navTo("RouteThirdparty");
		},

		onPress(oEvent) {
			MessageToast.show(`${oEvent.getSource().getId()} pressed`);
		},
		onBoo() {
			MessageToast.show(`👻`);
		},

		onPush(oEvent) {
			this.isSubscribed = (this.checked === true);
			if (this.isSubscribed) {
				this.unsubscribePush();
			}
			else {
				// this.subscribePush();
				this.subscribeUser();
			}
		},

		urlB64ToUint8Array(base64String) {
			const padding = '='.repeat((4 - base64String.length % 4) % 4);
			const base64 = (base64String + padding)
				.replace(/\-/g, '+')
				.replace(/_/g, '/');

			const rawData = window.atob(base64);
			const outputArray = new Uint8Array(rawData.length);

			for (let i = 0; i < rawData.length; ++i) {
				outputArray[i] = rawData.charCodeAt(i);
			}
			return outputArray;
		},

		initializeUI() {
			// Set the initial subscription value
			this.swRegistration.pushManager.getSubscription()
				.then((subscription) => {
					this.isSubscribed = !(subscription === null);

					if (this.isSubscribed) {
						MessageToast.show('User IS subscribed.');
					} else {
						MessageToast.show('User is NOT subscribed.');
					}

				});
		},

		updateSubscriptionOnServer(subscription) {
			MessageToast.show('update Subscription On Server.');

			const subscriptionJson = this.getView().byId('json')

			if (subscription) {
				subscriptionJson.setValue(JSON.stringify(subscription));
			} else {
			}
		},

		subscribeUser() {
			const applicationServerKey = this.urlB64ToUint8Array(this.applicationServerPublicKey);
			this.swRegistration.pushManager.subscribe({
				userVisibleOnly: true,
				applicationServerKey: applicationServerKey
			})
				.then((subscription) => {
					MessageToast.show('User is subscribed.');
					this.updateSubscriptionOnServer(subscription);
					this.isSubscribed = true;
				})
				.catch((err) => {
					MessageToast.show('Failed to subscribe the user: ', err);
				});
		},

		registerSW() {
			if ('serviceWorker' in navigator && 'PushManager' in window) {
				console.log('Service Worker and Push is supported');

				navigator.serviceWorker.register('./lib/sw.js')
					.then(
						(swReg) => {
							console.log('Service Worker is registered', swReg);
							MessageToast.show('Service Worker is registered');

							this.swRegistration = swReg;
							this.initializeUI()
						})
					.catch((error) => {
						console.error('Service Worker Error', error);
					});
			} else {
				console.warn('Push messaging is not supported');
				MessageToast.show('Push Not Supported');
			}
		}
	});
});
