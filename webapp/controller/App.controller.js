sap.ui.define(["test/Sample/controller/BaseController"], (Controller) => {
	"use strict";

	return Controller.extend("test.Sample.controller.App", {
		onInit() {
			console.log("Statement will be removed if transpile task is configured accordingly.");

			// Build file will have baseUrl as localhost:2000
			// replaced from ui5.yaml file with UI5 task ui5-task-stringreplacer
			var baseUrl = "BASE_URL_PLACEHOLDER";
			var randomTextToReplace = "some.deeply.nested.ANOTHER_PLACEHOLDER";
			var version = "${project.version}";
			console.log(baseUrl, randomTextToReplace, version);
			this._requestNotificationPermissions()

		},

		_requestNotificationPermissions() {
			let result = Notification.requestPermission()
			result.then(() => {
				if (Notification.permission === "granted") {
					const noti = new Notification('Hello!', {
						body: 'thanks'
					});
					// noti.onshow = () => alert('onShow');
					noti.onclick = () => alert('clicked');
				} else if (Notification.permission === "denied") {
					const noti = new Notification('Hello!', {
						body: 'sorry'
					});
				} else if (Notification.permission === "default") {
					const noti = new Notification('Hello!', {
						body: 'dont know'
					});
				}
			})
		},
	});
});
